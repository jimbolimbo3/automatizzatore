# Il Pacificatore

Basta far partire **ilpacificatore.bat** con permessi di amministratore

Chrome Kiosk va avviato così:

```cmd

"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --no-default-browser-check --no-first-run --disable-infobars --incognito --disable-session-crashed-bubble --overscroll-history-navigation=0 --kiosk http://freeswitch.keytech.it:7780/FindYourWay/Totem?Totem=D5-PS28 --user-data-dir=C:\Temp

```

autologon.exe è preso da qui:

https://docs.microsoft.com/en-us/sysinternals/downloads/autologon

Invece nowinupdate è preso da qui:

https://stackoverflow.com/questions/44555223/turn-off-windows-update-service-auto-updates-windows-10-using-powershell

Invece blocktelemetry.ps1 è da qui:

https://github.com/W4RH4WK/Debloat-Windows-10/blob/master/scripts/block-telemetry.ps1